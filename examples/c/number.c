// http://www.linfo.org/create_c2.html
// https://stackoverflow.com/questions/979141/how-to-programmatically-cause-a-core-dump-in-c-c

#include <stdio.h>
#include <signal.h>

int main() {

    // read a number from stdin
    int count;
    printf("Please enter a number: ");
    scanf("%d", &count);
    printf("The number is %d \n", count);

    // change the value
    count = 111111111;
    printf("The number is changed to %d \n", count);

    // force a core dump and abort
    raise (SIGABRT);
}

// compile: gcc -o number number.c
// enable core dumps: ulimit -c unlimited
// disable core dumps ulimit -c 0
