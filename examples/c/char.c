// http://www.linfo.org/create_c2.html
// https://c-for-dummies.com/blog/?p=1112
// https://stackoverflow.com/questions/979141/how-to-programmatically-cause-a-core-dump-in-c-c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

int main(int argc, char *argv[])
{
    // read line from stdin using the getline function
    char *buffer;
    size_t bufsize = 32;
    size_t characters;

    buffer = (char *)malloc(bufsize * sizeof(char));
    if( buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
    characters = getline(&buffer,&bufsize,stdin);
    printf("%zu characters were read.\n",characters);
    printf("You typed: '%s'\n",buffer);

    // remove trailing \n
    // https://stackoverflow.com/questions/2693776/removing-trailing-newline-character-from-fgets-input
    buffer[strcspn(buffer, "\n")] = 0;
    printf("The password is: %s \n", buffer);
    
    
    for(int i = 0; i < strlen(buffer); i++)
    {
    	buffer[i] = 'x';
    }

    printf("The password is now: %s \n", buffer);

    // force a core dump and abort
    raise (SIGABRT);
}

// compile: gcc -o number number.c
// enable core dumps: ulimit -c unlimited
// disable core dumps ulimit -c 0
