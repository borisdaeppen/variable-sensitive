use feature 'say';

be_gone();
dump;

sub be_gone {
    my $sensitive = <STDIN>;
    say "var is: $sensitive";
    $sensitive = 'xxxxxxxxxxx';
    say "var is: $sensitive";
    undef $sensitive;
    say "var is: $sensitive";
}
