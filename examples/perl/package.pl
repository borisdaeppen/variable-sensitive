use feature 'say';

Adieu::be_gone();
dump;


package Adieu;

sub be_gone {
    my $sensitive = <STDIN>;
    say "var is: $sensitive";
    $sensitive = 'xxxxxxxxxxx';
    say "var is: $sensitive";
    undef $sensitive;
    say "var is: $sensitive";
}

1;
